<?php

/**
 * @file
 *   The Index administration section.
 */

/**
 * List all existing indexes.
 */
function index_list() {
  drupal_add_css(drupal_get_path('module', 'index') . '/css/index.admin.css', 'module', 'all', FALSE);
  $header = array(
    t('Index'),
    array(
      'data' => t('Operations'),
      'colspan' => 4,
    ),
  );
  $contexts_info = index_contexts_info();
  $rows = array();
  $result = db_query("SELECT iid FROM {index_index} ORDER BY title");
  while ($iid = db_result($result)) {
    $index = index_load($iid);
    $contexts = array();
    foreach ($index->contexts as $context) {
      if ($context->enabled) {
        $title = $contexts_info[$context->name]['#title'];
        $contexts[] = $context->path ? l($title, $context->path) : $title;
      }
    }
    $created = (empty($contexts) ? NULL: ' (' . implode(', ', $contexts) . ')');
    $description = ($index->description ? '<div class="description">' . $index->description . '</div>' : NULL);
    $rows[] = array(
      '<strong>' . $index->title . '</strong>' . $created . $description,
      array(
        'data' => l(t('edit'), 'admin/build/index/edit/' . $index->iid),
        'class' => 'index-list-operation'
      ),
      array(
        'data' => l(t('export'), 'admin/build/index/export/' . $index->iid),
        'class' => 'index-list-operation'
      ),
      array(
        'data' => l(t('clone'), 'admin/build/index/clone/' . $index->iid),
        'class' => 'index-list-operation'
      ),
      array(
        'data' => l(t('delete'), 'admin/build/index/delete/' . $index->iid),
        'class' => 'index-list-operation'
      ),
    );
  }
  if (!count($rows)) {
    $rows[] = array(array(
      'data' => t('There are no indexes. <a href="!index_form_add">Add a new one</a>.', array('!index_form_add' => url('admin/build/index/add'))),
      'colspan' => 5,
    ));
  }

  return theme('table', $header, $rows);
}

/**
 * Form: the index add form.
 *
 * @return array
 *   A Drupal form.
 */
function index_form_add(array $form_state, index $index_existing = NULL) {
  drupal_add_css(drupal_get_path('module', 'index') . '/css/index.admin.css', 'module', 'all', FALSE);
  drupal_add_css(drupal_get_path('module', 'index') . '/css/index.view.css', 'module', 'all', FALSE);
  drupal_add_js(drupal_get_path('module', 'index') . '/js/index.admin.js');
  drupal_add_js(drupal_get_path('module', 'index') . '/js/index.view.js');

  index_load_include('view');

  // Load and populate the index.
  // An existing index is being edited.
  if ($index_existing) {
    drupal_set_title(t('Edit %title', array('%title' => $index_existing->title)));
    $index_edited = clone $index_existing;
  }
  // A new index is being created.
  else {
    index_load_include('classes');
    $index_existing = new index();
    $index_edited = clone $index_existing;
    index_form_add_populate_index_new($index_edited);
  }
  // The form has been rebuilt.
  if (isset($form_state['values'])) {
    index_form_add_populate_index_rebuild($index_edited, $form_state);
  }

  $form = array(
    '#cache' => TRUE,
    '#redirect' => 'admin/build/index',
  );

  // Internal settings.
  $form['iid'] = array(
    '#type' => 'value',
    '#value' => $index_existing->iid,
  );
  $form['index_existing'] = array(
    '#type' => 'value',
    '#value' => $index_existing,
  );
  $form['index_edited'] = array(
    '#type' => 'value',
    '#value' => $index_edited,
  );
  // The names of the callback functions that populate the index with form
  // values after submission.
  $form['rebuild'] = array(
    '#type' => 'value',
    '#value' => array(),
  );

  // Index identification.
  $form['identification'] = array (
    '#prefix' => '<div id="index-form-add-identification">',
    '#suffix' => '</div>',
  );
  $form['identification']['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => $index_edited->title,
    '#maxlength' => 255,
    '#size' => 30,
    '#required' => TRUE,
  );
  $form['identification']['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => $index_edited->description,
    '#cols' => 30,
  );
  // General settings.
  $form['general'] = array (
    '#prefix' => '<div id="index-form-add-general">',
    '#suffix' => '</div>',
  );
  $form['general']['language'] = array(
    '#type' => 'select',
    '#title' => t('Language'),
    '#options' => index_language_options(),
    '#default_value' => $index_edited->language,
  );
  $timestamps = array(60, 120, 180, 240, 300, 600, 1200, 1800, 2400, 3000, 3600, 7200, 10800, 14400, 18000, 86400);
  $options = array(0 => t('Do not cache'));
  foreach ($timestamps as $timestamp) {
    $options[$timestamp] = format_interval($timestamp, 5);
  }
  $form['general']['cache_lifetime'] = array(
    '#type' => 'select',
    '#title' => t('Cache lifetime'),
    '#options' => $options,
    '#default_value' => $index_edited->cache_lifetime,
  );

  // Contexts.
  $form['contexts'] = array(
    '#type' => 'fieldset',
    '#title' => t('Create index as'),
    '#attributes' => array(
      'id' => 'index-contexts',
    ),
  );
  $displays_info = index_displays_info();
  $contexts_info = index_contexts_info();
  $js_contexts_info = array();
  foreach ($contexts_info as $context_name => $context_info) {
    $js_contexts_info[$context_name] = $context_info['#title'];
    $form['contexts'][$context_name] = array(
      '#type' => 'fieldset',
      '#title' => $context_info['#title'],
      '#attributes' => array(
        'id' => 'index-context-' . $context_name,
      ),
      );
    $form['contexts'][$context_name]['preview'] = array(
      '#value' => '<div id="index-context-preview-' . $context_name . '" class="index-context-preview"><h2>' . t('Preview') . '</h2><div class="content">' . index_preview($index_edited, $context_name) . '</div></div>',
    );
    $form['contexts'][$context_name]['context_enabled_' . $context_name] = array(
      '#type' => 'checkbox',
      '#title' => t('Create index as a %context', array('%context' => $context_info['#title'])),
      '#default_value' => $index_edited->contexts[$context_name]->enabled,
      '#attributes' => array(
        'class' => 'index-context-enabled',
      ),
    );
  	$form['contexts'][$context_name]['settings'] = array(
      '#prefix' => '<div class="index-context-settings">',
      '#suffix' => '</div>',
  	);
    $display_options = array();
    foreach ($displays_info as $display_name => $display_info) {
      if (in_array($context_name, $display_info['#contexts'])) {
        $display_options[$display_name] = $display_info['#title'];
      }
    }
    $form['contexts'][$context_name]['settings']['context_display_' . $context_name] = array(
      '#type' => 'select',
      '#title' => t('Display mode'),
      '#options' => $display_options,
      '#default_value' => $index_edited->contexts[$context_name]->display,
    );
  }
  drupal_add_js(array('indexContexts' => $js_contexts_info), 'setting');

  // Layers widget.
  $form['layers'] = index_form_add_layers($index_edited, $form_state);

  $form['buttons']['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#submit' => array('index_form_add_save'),
  );
  $form['buttons']['update_preview'] = array(
    '#type' => 'submit',
    '#value' => t('Update preview'),
    '#submit' => array('index_form_add_update'),
  );
  $form['buttons']['delete'] = array(
    '#type' => 'markup',
    '#value' => l(t('Delete'), 'admin/build/index/delete/' . $index_existing->iid),
  );

  return $form;
}

/**
 * Validate handler for index_form_add().
 */
function index_form_add_validate_context_page(array $form, array &$form_state) {
  $values = $form_state['values'];
  if ($values['context_enabled_page'] && !$values['context_path_page']) {
    form_set_error('context_path_page', t('Please enter a path for the index page.'));
  }
  // Assure that the page path isn't already in use.
  $page_path = $values['context_path_page'];
  // If the path is empty, return to prevent form errors from popping up.
  if (empty($page_path)) {
  return;
  }
  // Prepare the path for validation.
  $page_path = trim($page_path, '/');
  form_set_value($form['contexts']['page']['settings']['context_path_page'], $page_path, $form_state);
  // Check for illegal characters.
  if (strpos($page_path, '%') !== FALSE) {
    form_set_error('context_path_page', t('Paths may not contain percentage signs.'));
  }
  // Check if the path is already being used for this index.
  elseif (!db_result(db_query("SELECT COUNT(*) FROM {index_context_page_path} WHERE iid = %d AND path = '%s'", $values['iid'], $page_path))) {
    // Check for existing menu paths.
    $count_menu_links = db_result(db_query("SELECT COUNT(*) FROM {menu_links} WHERE link_path = '%s'", $page_path));
    if ($count_menu_links) {
      form_set_error('context_path_page', t('Path is already used by a menu link.'));
    }
    // Check for existing aliases.
    elseif (drupal_lookup_path('source', $page_path)) {
      form_set_error('context_path_page', t('Path is already used as a URL alias.'));
    }
  }
}

/**
 * Submit handler for index_form_add().
 *
 * Save user submitted data.
 */
function index_form_add_save(array $form, array &$form_state) {
  $values = $form_state['values'];
  index_load_include('classes');
  $index = new index();
  index_form_add_populate_index_rebuild($index, $form_state);

  if ($values['iid']) {
    $index->update();
    drupal_set_message(t('%title has been updated.', array('%title' => $index->title)));
  }
  else {
    $index->insert();
    drupal_set_message(t('%title has been created.', array('%title' => $index->title)));
  }
}

/**
 * Submit handler for index_form_add().
 *
 * Rebuild index_form_add() to process user submitted data.
 */
function index_form_add_update(array $form, array &$form_state) {
  $form_state['rebuild'] = TRUE;
}

/**
 * Update index_form_add() using AJAX.
 */
function index_form_add_update_ajax() {
  // Build a fake $form_state and get the form.
  $form_state = array(
    'storage' => NULL,
    'submitted' => FALSE,
  );
  $form_build_id = $_POST['form_build_id'];
  index_load_include('classes');
  $form = form_get_cache($form_build_id, $form_state);

  // Process and rebuild the form.
  $args = $form['#parameters'];
  $form_id = array_shift($args);
  $form_state['post'] = $form['#post'] = $_POST;
  $form['#programmed'] = $form['#redirect'] = FALSE;
  drupal_process_form($form_id, $form, $form_state);
  $form = drupal_rebuild_form($form_id, $form_state, $args, $form_build_id);
  ob_start();

  // Suppress (validation) errors.
  form_set_error(NULL, '', TRUE);
  unset($_SESSION['messages']['error']);

  $previews = array();
  foreach (array_keys(index_contexts_info()) as $context_name) {
    $previews[$context_name] = index_preview($form['index_edited']['#value'], $context_name);
  }
  drupal_json(array(
    'layers' => strpos($form_state['clicked_button']['#name'], 'layer_') !== FALSE ? theme('index_form_add_layers', $form['layers']) : NULL,
    'previews' => $previews,
  ));
}

/**
 * Theme index_form_add().
 */
function theme_index_form_add(array $form) {
  // Theme layers and unset them so they aren't rendered twice.
  $layers = theme('index_form_add_layers', $form['layers']);
  unset($form['layers']);

  return drupal_render($form['identification']) . drupal_render($form['general']) . drupal_render($form['contexts']) . $layers . drupal_render($form);
}

/**
 * Theme index_form_add()'s hierarchy selectors.
 */
function theme_index_form_add_layers(array $form) {
  $rows = array();
  foreach ($form['lineages']['#value'] as $lineage) {
    $depth = count(explode('_', $lineage)) - 1;
    $rows[] = array(
      theme('indentation', $depth - 1) . theme('index_form_add_layer_depth', $depth) . (isset($form["layer_type_item-$lineage"]) ? drupal_render($form["layer_type_item-$lineage"]) : drupal_render($form["layer_type-$lineage"])),
      array(
        'data' => drupal_render($form["layer_entity_count_only-$lineage"]),
        'class' => 'checkbox',
      ),
      array(
        'data' => drupal_render($form["layer_hide_childless_entities-$lineage"]),
        'class' => 'checkbox',
      ),
      drupal_render($form["layer_child_add-$lineage"]),
      drupal_render($form["layer_remove-$lineage"]),
    );
  }
  $rows[] = array(
    array(
      'data' => drupal_render($form['layer_child_add']),
      'colspan' => 5,
    ),
  );
  $header = array(
    t('Type'),
    t('Count items only'),
    t('Hide childless items'),
    array(
      'data' => t('Operations'),
      'colspan' => 2,
    ),
  );

  $warning = isset($form['warning']) ? drupal_render($form['warning']) : NULL;

  return '<div id="index-form-add-layers">' . theme('table', $header, $rows, array(), t('Hierarchy')) . $warning . '</div>';
}

/**
 * Theme the layer depth icon.
 *
 * @param $depth integer
 *   The depth of the layer to theme the depth icon for.
 */
function theme_index_form_add_layer_depth($depth) {
  $root = !$depth ? 'root' : NULL;

  return '<div class="index-form-add-layer-depth ' . $root . '"></div>';
}

/**
 * Populate an index with form submitted data after a form rebuild.
 *
 * @param $index index
 *   The index object to populate.
 */
function index_form_add_populate_index_new(index $index) {
  $index->layers[] = new indexLayer(array(
    'lineage' => '0',
    'index' => $index,
    'entity_type' => array_shift(array_keys(index_entity_types_info())),
  ));
}

/**
 * Populate an index with form submitted data after a form rebuild.
 *
 * @param $index index
 *   The index object to populate.
 * @param $form_state array
 *   The $form_state of the form from which use get the data. 
 */
function index_form_add_populate_index_rebuild(index $index, array &$form_state) {
  $values = $form_state['values'];

  // Identification and general settings.
  $index->iid = $values['iid'];
  $index->title = $values['title'];
  $index->description = $values['description'];
  $index->language = $values['language'];
  $index->cache_lifetime = $values['cache_lifetime'];

  // Contexts.
  foreach (index_contexts_info() as $context_name => $context_info) {
    $index->contexts[$context_name]->enabled = $values['context_enabled_' . $context_name];
    $index->contexts[$context_name]->display = $values['context_display_' . $context_name];
  }

  $lineages = array();
  $layers = array();
  // Add child layers and remove existing ones based on user input.
  if (isset($form_state['clicked_button']) && strpos($form_state['clicked_button']['#name'], 'layer_') === 0) {
    $clicked_button = $form_state['clicked_button']['#name'];
    $clicked_chunks = explode('-', $clicked_button);
    $clicked_action = array_shift($clicked_chunks);
    $clicked_lineage = array_shift($clicked_chunks);
    // Add a new child layer.
    $new_lineage = NULL;
    if ($clicked_action == 'layer_child_add') {
      // We set -1, because if there are no layers max(-1) + 1 returns 0 as
      // (part of) the new layer's lineage.
      $sibling_lineages = array(-1);
      $new_lineage = '';
      // Compute the new layer's lineage if the new layer is in the root.
      if (!strlen($clicked_lineage)) {
        foreach ($values['lineages'] as $lineage) {
          if (strpos($lineage, '_') === FALSE) {
            $sibling_lineages[] = $lineage;
          }
        }
        $lineages[] = $new_lineage = max($sibling_lineages) + 1;
      }
      // Compute the new layer's lineage if the layer has a parent.
      else {
        $count_children = 0;
        foreach ($values['lineages'] as $lineage) {
          if (preg_match('#' . $clicked_lineage . '_\d+?#', $lineage)) {
            $sibling_lineages[] = substr($lineage, strrpos($lineage, '_') + 1);
          }
        }
        $lineages[] = $new_lineage = $clicked_lineage . '_' . (max($sibling_lineages) + 1);
      }
    }
  }
  // Create layers for every known lineage, either existing or new.
  $new_layer = NULL;
  foreach (array_merge($values['lineages'], $lineages) as $lineage) {;
    $lineage = (string) $lineage;
    // Don't save this layer and its children into the index if it has been
    // removed.
    if (isset($clicked_action) && $clicked_action == 'layer_remove' && ($lineage === $clicked_lineage || strpos($lineage, $clicked_lineage . '_') === 0)) {
      continue;
    }
    // Add the current layer.
    $lineages[] = $lineage;
    $layers[$lineage] = new indexLayer(array(
      'lineage' => $lineage,
      'entity_type' => isset($values["layer_type-$lineage"]) ? $values["layer_type-$lineage"] : NULL,
      'entity_count_only' => isset($values["layer_entity_count_only-$lineage"]) ? $values["layer_entity_count_only-$lineage"] : 0,
      'hide_childless_entities' => isset($values["layer_hide_childless_entities-$lineage"]) ? $values["layer_hide_childless_entities-$lineage"] : 0,
    ));
    // Store the newly added layer so we can set its entity type later.
    if (isset($clicked_action) && $lineage === strval($new_lineage)) {
      $new_layer = $layers[$lineage];
    }
  }
  $values['lineages'] = $lineages;
  $index->setLayers($layers);
  if ($new_layer) {
    // Set the newly added layer's entity type. This can only be done after the
    // layers are set, because it requires the parent and sibling layers to be
    // known.
    $new_layer->entity_type = array_shift(array_keys(index_existing_layer_entity_type_options($new_layer)));
  }

  // Populate the index using functions set in $form['rebuild'].
  foreach ($form_state['values']['rebuild'] as $function) {
    call_user_func($function, $index, $form_state);
  }
}

/**
 * Populate an index with form submitted context-specific data after a form
 * rebuild.
 *
 * @param $index index
 *   The index object to populate.
 * @param $form_state array
 *   The $form_state of the form from which use get the data. 
 */
function index_form_add_populate_index_rebuild_context_page(index $index, array $form_state) {
	$index->contexts['page']->path = $form_state['values']['context_path_page'];
	$index->contexts['page']->roles = array();
	foreach ($form_state['values']['context_roles_page'] as $rid => $value) {
		if ($value) {
			$index->contexts['page']->roles[] = $rid;
		}
	}
}

/**
 * Create the layer building widget, by default a table.
 *
 * @param $index index
 *   The index the form is being built for.
 * @param $form_state array
 *   The form's $form_state.
 *
 * @return array
 *   The form elements for the layer building widget.
 */
function index_form_add_layers(index $index, array $form_state) {
  $disabled = (bool) !index_fake_layer_entity_type_options($index);
  $form['layer_child_add'] = array(
    '#type' => 'submit',
    '#value' => t('Add item'),
    '#submit' => array('index_form_add_update'),
    '#disabled' => $disabled,
    '#attributes' => array(
      'class' => 'index-form-add-layer-child-add' . ($disabled ? ' disabled' : NULL),
      'title' => t('Add item'),
    ),
    // By default FAPI gives every button name="op", which makes it
    // impossible to detect the clicked button if multiple buttons have the
    // same value. To fix this we configure a custom and unique name
    // (undocumented feature).
    '#name' => 'layer_child_add',
  );

  $form['lineages'] = array(
    '#type' => 'value',
    '#value' => array(),
  );

  _index_form_add_layers($form, $index->layers);

  if (isset($form_state['rebuild']) && $form_state['rebuild']) {
    $form['warning'] = array(
      '#type' => 'item',
      '#value' => '<div class="warning">' . t('Changes will not take effect until the index is saved.') . '</div>',
      );
  }

  return $form;
}

/**
 * Create part of the layer building widget.
 *
 * @param $form array
 *   The form to add the new elements to.
 * @param $layers array
 *   The layers to add form elements for.
 * @param $parent_entity_type string
 *   The entity type of the parent layer. Used to compute the available entity
 *   types.
 */
function _index_form_add_layers(array &$form, array $layers, $parent_entity_type = NULL) {
  foreach ($layers as $layer) {
    if (count($layer->layers)) {
      // Parent layers cannot be modified and are represented by a value element
      // for processing and an item element for the user to see.
      $form['layer_type-' . $layer->lineage] = array(
        '#type' => 'hidden',
        '#value' => $layer->entity_type,
      );
      $entity_type_info = index_entity_type_info($layer->entity_type);
      $form['layer_type_item-' . $layer->lineage] = array(
        '#type' => 'item',
        '#value' => $entity_type_info['#title'],
      );
      // If there are no child layers there are no child entities to count.
      $form['layer_hide_childless_entities-' . $layer->lineage] = array(
        '#type' => 'checkbox',
        '#default_value' => $layer->hide_childless_entities,
      );
    }
    // Layers that have no children can be modified.
    else {
      $layer_entity_type_options = index_existing_layer_entity_type_options($layer);
      $form['layer_type-' . $layer->lineage] = array(
        '#type' => 'select',
        '#options' => $layer_entity_type_options,
        // Unconfigured layers get the first of the available values as their
        // default value.
        '#default_value' => !is_null($layer->entity_type) ? $layer->entity_type : array_shift(array_keys($layer_entity_type_options)),
        '#disabled' => (bool) !index_fake_layer_entity_type_options(isset($layer->parent) ? $layer->parent : $layer->index),
      );
      // Entity count only should never be available for root layers.
      if ($layer->parent) {
        $form['layer_entity_count_only-' . $layer->lineage] = array(
          '#type' => 'checkbox',
          '#default_value' => $layer->entity_count_only,
        );
      }
    }
    $disabled_child_add = (bool) !index_fake_layer_entity_type_options($layer);
    $form['layer_child_add-' . $layer->lineage] = array(
      '#type' => 'submit',
      '#value' => t('Add item'),
      '#submit' => array('index_form_add_update'),
      '#disabled' => $disabled_child_add,
      '#attributes' => array(
        'class' => 'index-form-add-layer-child-add' . ($disabled_child_add ? ' disabled' : NULL),
        'title' => t('Add item'),
      ),
      // By default FAPI gives every button name="op", which makes it
      // impossible to detect the clicked button if multiple buttons have the
      // same value. To fix this we configure a custom and unique name
      // (undocumented feature).
      '#name' => 'layer_child_add-' . $layer->lineage,
    );
    // If there is only one layer it cannot be removed.
    $disabled_remove = count($layer->index->layers) == 1 && strpos($layer->lineage, '_') === FALSE;
    $form['layer_remove-' . $layer->lineage] = array(
      '#type' => 'submit',
      '#value' => t('Remove'),
      '#submit' => array('index_form_add_update'),
      '#disabled' => $disabled_remove,
      '#attributes' => array(
        'class' => 'index-form-add-layer-remove' . ($disabled_remove ? ' disabled' : NULL),
        'title' => t('Remove this item and its children'),
      ),
      // By default FAPI gives every button name="op", which makes it
      // impossible to detect the clicked button if multiple buttons have the
      // same value. To fix this we configure a custom and unique name
      // (undocumented feature).
      '#name' => 'layer_remove-' . $layer->lineage,
    );

    $form['lineages']['#value'][] = $layer->lineage;
    _index_form_add_layers($form, $layer->layers, $layer->entity_type);
  }
}

/**
 * Compute options for the entity type form element.
 *
 * @return array
 *   An array where keys are the entity types' machine names and values are
 *   their titles.
 */
function index_layer_entity_type_options() {
  static $entity_type_options = NULL;

  if (!$entity_type_options) {
    $entity_types_info = index_entity_types_info();
    foreach ($entity_types_info as $entity_type => $entity_type_info) {
      $entity_type_options[$entity_type] = $entity_type_info['#title'];
    }
  }

  return $entity_type_options;  
}

/**
 * Compute possible entity types for an existing layer.
 *
 * @param $layer indexLayer
 *
 * @return array
 *   An array with entity type machine names and titles as keys and values.
 */
function index_existing_layer_entity_type_options(indexLayer $layer) {
  $entity_type_options = index_layer_entity_type_options();

  $family_entity_types = array();
  // There is a parent layer, so use its children as siblings.
  if ($layer->parent) {
    $family_entity_types[] = $layer->parent->entity_type;
    $sibling_layers = $layer->parent->layers;
  }
  // There is no parent layer, meaning we are computing entity type options for
  // a root layer. Use all root layers as siblings.
  else {
    $sibling_layers = $layer->index->layers;
  }

  foreach ($sibling_layers as $sibling_layer) {
    // The current layer is no sibling.
    if ($layer->lineage != $sibling_layer->lineage) {
      $family_entity_types[] = $sibling_layer->entity_type;
    }
  }
  if ($layer->parent) {
    $entity_type_info = index_entity_type_info($layer->parent->entity_type);
    $relations = $entity_type_info['#relations'];
  }
  else {
    $relations = $entity_type_options;
  }

  return array_diff_key(array_intersect_key($entity_type_options, $relations), array_flip($family_entity_types));
}

/**
 * Compute possible entity types for a fake layer.
 *
 * @param $parent index, indexLayer
 *   The fake layer's parent, either another layer or the index it belongs to.
 *
 * @return array
 *   An array with entity type machine names and titles as keys and values.
 */
function index_fake_layer_entity_type_options($parent) {
  $entity_type_options = index_layer_entity_type_options();

  $family_entity_types = array();
  if (isset($parent->entity_type)) {
    $entity_type_info = index_entity_type_info($parent->entity_type);
    $family_entity_types[] = $parent->entity_type;
  }

  foreach ($parent->layers as $sibling_layer) {
    $family_entity_types[] = $sibling_layer->entity_type;
  }

  $relations = isset($parent->entity_type) ? $entity_type_info['#relations'] : $entity_type_options;

  return array_diff_key(array_intersect_key($entity_type_options, $relations), array_flip($family_entity_types));
}

/**
 * Preview an index.
 *
 * @param $index index
 *   The index to preview.
 * @param $context_name string
 *   The name of the context to check access for.
 *
 * @return string
 */
function index_preview(index $index, $context_name) {
  if ($index->contexts[$context_name]->enabled && $index->contexts[$context_name]->display) {
     return $index->contexts[$context_name]->view(TRUE);
  }
  else {
    $context_info = index_context_info($context_name);
    return '<div class="warning">' . t('The index has not been enabled as a %context.', array('%context' => $context_info['#title'])) . '</div>';
  }
}

/**
 * Form: the index delete form.
 *
 * @return array
 *   A Drupal form.
 */
function index_form_delete(array &$form_state, index $index) {
  $form['#redirect'] = 'admin/build/index';
  $form['iid'] = array(
    '#type' => 'value',
    '#value' => $index->iid,
  );

  return confirm_form($form,
    t('Are you sure you want to delete %title?', array('%title' => $index->title)),
    isset($_GET['destination']) ? $_GET['destination'] : 'admin/build/index',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Submit handler for index_form_delete().
 */
function index_form_delete_submit(array $form, array &$form_state) {
  $index = index_load($form_state['values']['iid']);
  $index->delete();
  drupal_set_message(t('%title has been deleted.', array('%title' => $index->title)));
}

/**
 * Export an index as a file download.
 *
 * @param $index index
 *   The index to export.
 */
function index_export(index $index) {
  $blueprint = index_blueprint($index);
  drupal_set_header('Content-Type: text/plain; charset=UTF-8');
  drupal_set_header('Content-Disposition: attachment; filename=' . index_export_filename($index->title));
  echo serialize($blueprint);
}

/**
 * Return an array with characters that cannot appear in filenames.
 *
 * @return array
 */
function index_export_filename_illegal_characters() {
  return array(' ', '/', '\\', '?', '%', '*', ':', '|', '\"', '<', '>', '.');
}

/**
 * Create the filename for an exported index.
 *
 * @param $source string
 *   The string to use for the filename. The index' title for instance.
 */
function index_export_filename($source) {
  return 'index-' . str_replace(index_export_filename_illegal_characters(), '_', strtolower(trim($source))) . '.txt';
}

/**
 * The index import form.
 *
 * @return array
 *   A Drupal form.
 */
function index_form_import(array $form_state) {
  $form = array(
    '#attributes' => array(
      'enctype' => 'multipart/form-data',
    ),
    '#redirect' => 'admin/build/index',
  );
  $form['file'] = array(
    '#type' => 'file',
    '#title' => t('File'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import'),
  );

  return $form;
}

/**
 * Validate handler for index_form_import().
 */
function index_form_import_validate(array $form, array &$form_state) {
  index_load_include('classes');
  $error = FALSE;
  // Check if the imported file contains serialized data and if the
	// unserialized data is an index object.
  if (!($index = @unserialize(file_get_contents($_FILES['files']['tmp_name']['file']))) || !($index instanceof index)) {
    form_set_error('file', t("The imported file doesn't contain an exported index."));
  }
}

/**
 * Validate handler for index_form_import().
 */
function index_form_import_submit(array $form, array &$form_state) {
  index_load_include('classes');
  $index = unserialize(file_get_contents($_FILES['files']['tmp_name']['file']));
  $index->insert();
  drupal_set_message(t('Index %index_title has been imported.', array('%index_title' => $index->title)));
}

/**
 * Clone an index.
 *
 * @param $index index
 *   The index to clone.
 */
function index_clone(index $index) {
  $blueprint = index_blueprint($index);
  $blueprint->title = $index->title . ' (' . t('cloned') . ')';
  $blueprint->insert();
  drupal_set_message(t('Index %index_original_title has been cloned to %index_clone_title.', array('%index_original_title' => $index->title, '%index_clone_title' => $blueprint->title)));
  drupal_goto('admin/build/index');
}

/**
 * Return a list of languages.
 *
 * @return
 *   Array where the keys are languages codes and values are language names.
 */
function index_languages() {
  static $languages = NULL;
  include_once './includes/locale.inc';

  if(!$languages) {
    $languages = _locale_get_predefined_list();
    foreach ($languages as $code => $names) {
      // Include native name in output, if possible
      $name = t($names[0]);
      $languages[$code] = $name;
      if (count($names) > 1 && $name != $names[1]) {
        $languages[$code] .= ' (' . $names[1] . ')';
      }
    }
    asort($languages);
  }

  return $languages;
}

/**
 * Return the form element options for an index' language.
 *
 * @return array
 */
function index_language_options() {
  return array(
    INDEX_LANGUAGE_NEUTRAL => t('Language neutral'),
    INDEX_LANGUAGE_ACTIVE => t('Active interface language'),
    t('Languages') => index_languages(),
  );
}