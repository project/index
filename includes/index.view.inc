<?php

/**
 * @file
 *   All functions for viewing indexes.
 */

/**
 * View an index.
 *
 * @param $iid integer
 *   The IID of the index to view.
 * @param $context string
 *   The machine name of the context to view the index for.
 *
 * @return
 *   The rendered index.
 */
function index_view($iid, $context) {
  $index = index_load($iid);

  return $index->contexts[$context]->view();
}

/**
 * Theme an index.
 */
function theme_index(index $index, indexContext $context, $content) {
  return '<div class="index index-display-' . $context->display . '">' . $index->description . $content . '</div>';
}

/**
 * Theme a general list style placeholder.
 *
 * Usage examples include clickable list produced by theme_index_display_tree().
 */
function theme_index_list_style() {
  return '<span class="index-list-style"></span>';
}

/**
 * 'Theme' an entity's child count.
 */
function index_child_count(indexEntity $entity) {
  global $language;

  $dir = defined('LANGUAGE_RTL') && $language->direction == LANGUAGE_RTL ? 'rtl' : 'ltr';

  return $entity->child_count ? ' (' . $entity->child_count . ')' : NULL;
}

/**
 * Theme a user.
 */
function theme_index_entity_user(indexEntityUser $entity) {
  $title = $entity->title() . index_child_count($entity);

  if ($entity->path) {
    return l($title, $entity->path);
  }
  else {
    return $title;
  }
}

/**
 * Theme a user role.
 */
function theme_index_entity_user_role(indexEntity $entity) {
  $title = $entity->title() . index_child_count($entity);

  if ($entity->path) {
    return l($title, $entity->path);
  }
  else {
    return $title;
  }
}

/**
 * Theme a user role.
 */
function theme_index_entity_language(indexEntity $entity) {
  $title = $entity->title() . index_child_count($entity);

  if ($entity->path) {
    return l($title, $entity->path);
  }
  else {
    return $title;
  }
}

/**
 * Theme a file.
 */
function theme_index_entity_file(indexEntity $entity) {
  $title = $entity->title() . index_child_count($entity);

  if ($entity->path) {
    return l($title, $entity->path);
  }
  else {
    return $title;
  }
}

/**
 * Theme a node.
 */
function theme_index_entity_node(indexEntity $entity) {
  $title = $entity->title() . index_child_count($entity);

  if ($entity->path) {
    return l($title, $entity->path);
  }
  else {
    return $title;
  }
}

/**
 * Theme a node type.
 */
function theme_index_entity_node_type(indexEntity $entity) {
  $title = $entity->title() . index_child_count($entity);
  $description = $entity->data->description ? '<div class="description">' . $entity->data->description . '</div>' : NULL;

  if ($entity->path) {
    $title = l($title, $entity->path);
  }

  return $title . $description;
}

/**
 * Theme a comment.
 */
function theme_index_entity_comment(indexEntity $entity) {
  $title = $entity->title() . index_child_count($entity);

  if ($entity->path) {
    return l($title, $entity->path);
  }
  elseif (user_access('access comments')) {
    return l($title, 'node/' . $entity->data->nid, array('fragment' => 'comment-' . $entity->data->cid));
  }
  else {
    return $title;
  }
}

/**
 * Theme a Taxonomy term.
 */
function theme_index_entity_taxonomy_term(indexEntity $entity) {
  $title = $entity->title() . index_child_count($entity);
  $description = $entity->data->description ? '<div class="description">' . $entity->data->description . '</div>' : NULL;

  return l($title, $entity->path) . $description;
}

/**
 * Theme a Taxonomy vocabulary.
 */
function theme_index_entity_taxonomy_vocabulary(indexEntity $entity) {
  $title = $entity->title() . index_child_count($entity);
  $description = $entity->data->description ? '<div class="description">' . $entity->data->description . '</div>' : NULL;

  if ($entity->path) {
    $title = l($title, $entity->path);
  }

  return $title . $description;
}

/**
 * Render an index as a tree.
 *
 * @param $index index
 *   The index object of the index to render.
 * @param $context indexContext
 *   The context for which to render entities.
 *
 * @return string
 *   The fully rendered index.
 */
function theme_index_display_tree(index $index, indexContext $context) {
  drupal_add_css(drupal_get_path('module', 'index') . '/css/index.view.css', 'module', 'all', FALSE);

  return theme('item_list', _index_display_tree($index, $context->entities));
}

/**
 * Helper function for index_display_tree().
 */
function _index_display_tree(index $index, $entities, &$i = 0) {
  $list_style = theme('index_list_style');
  $items = array();
  foreach ($entities as $entity) {
    $i++;
    $children = _index_display_tree($index, $entity->children, $items, $i);
    $items[] = array(
      'data' => $list_style . $entity->theme(),
      'children' => $children,
      'class' => ($i % 2 == 0 ? 'even' : 'odd') . (!empty($children) ? ' parent expanded' : NULL),
    );
  }

  return $items;
}

/**
 * Render an index as a expandable tree.
 *
 * @param $index index
 *   The index object of the index to render.
 * @param $context indexContext
 *   The context for which to render entities.
 *
 * @return string
 *   The fully rendered index.
 */
function theme_index_display_tree_expandable(index $index, indexContext $context) {
  drupal_add_js(drupal_get_path('module', 'index') . '/js/index.view.js');

  return theme('index_display_tree', $index, $context);
}

/**
 * Render an index' upper layers.
 *
 * @param $index index
 *   The index object of the index to render.
 * @param $context indexContext
 *   The context for which to render entities.
 *
 * @return string
 *   The fully rendered index.
 */
function theme_index_display_upper(index $index, indexContext $context) {
  drupal_add_css(drupal_get_path('module', 'index') . '/css/index.view.css', 'module', 'all', FALSE);

  $items = array();
  $i = 0;
  foreach ($context->entities as $entity) {
    $i++;
    $items[] = array(
      'data' => $entity->theme(),
      'class' => $i % 2 == 0 ? 'even' : 'odd',
    );
  }

  return theme('item_list', $items, NULL, 'ul', array('class' => 'index-display-flat'));
}

/**
 * Render an index' two upper levels of layers.
 *
 * @param $index index
 *   The index object of the index to render.
 * @param $context indexContext
 *   The context for which to render entities.
 *
 * @return string
 *   The fully rendered index.
 */
function theme_index_display_upper_two(index $index, indexContext $context) {
  drupal_add_css(drupal_get_path('module', 'index') . '/css/index.view.css', 'module', 'all', FALSE);

  $items = array();
  $i = 0;
  foreach ($context->entities as $entity) {
    $children = array();
    foreach ($entity->children as $child_entity) {
      $children[] = $child_entity->theme();
    }
    $i++;
    $items[] = array(
      'data' => $entity->theme(),
      'children' => $children,
      'class' => $i % 2 == 0 ? 'even' : 'odd',
    ); 
  }

  return theme('item_list', $items, NULL, 'ul', array('class' => 'index-display-flat'));
}

/**
 * Render a browsable index.
 *
 * @param $index index
 *   The index object of the index to render.
 * @param $context indexContext
 *   The context for which to render entities.
 *
 * @return string
 *   The fully rendered index.
 */
function theme_index_display_browsable(index $index, indexContext $context) {
  drupal_add_css(drupal_get_path('module', 'index') . '/css/index.view.css', 'module', 'all', FALSE);

  $items = array();
  $i = 0;
  foreach ($context->entities as $entity) {
    $i++;
    $items[] = array(
      'data' => $entity->theme(),
      'class' => ($entity->child_count > 0 ? 'parent' : NULL) .  ($i % 2 == 0 ? ' even' : ' odd'),
    );
  }

  return theme('item_list', $items, NULL, 'ul', array('class' => 'index-display-flat'));
}

/**
 * Find the root entity for displaying an index.
 *
 * @param $index index
 *   The index to find the root entity for.
 *
 * @return mixed
 *   An aray where the first item is a refence to the root entity's layer and
 *   the second item is the entity's data ID (NID, TID, etc.) if a root entity
 *   has been found, FALSE if the requested page is the index' root or
 *   MENU_NOT_FOUND if a non-existing page was requested.
 */
function index_display_browsable_root_entity(index $index) {
  // Check if this index is being displayed at its page or somewhere else, a
  // preview for instance. In those cases there is no path we can depend on to
  // find the root entity.
  $page_path = $index->contexts['page']->path;
  if (strpos($_GET['q'], $page_path) === 0) {
    $selectors_path = trim(ltrim($_GET['q'], $page_path), '/');
    if ($selectors_path && $selectors = explode('/', $selectors_path)) {
      $entity_types_info = index_entity_types_info();

      // Start with the root layers.
      $layers = $index->layers;
      $root_layer = NULL;
      $root_entity_data_id = NULL;
      $breadcrumbs = array(
        l(t('Home'), '<front>'),
        l($index->title, $index->contexts['page']->path),
      );
      foreach ($selectors as $depth => $selector) {
        $found = FALSE;
        // Make sure the selector contains one hyphen as basic validation.
        $selector_parts = explode('-', $selector);
        if (count($selector_parts) == 2) {
          list($entity_type, $entity_data_id) = $selector_parts;
          foreach ($layers as $layer) {
            if ($layer->entity_type == $entity_type) {
              $root_layer = $layer;
              $root_entity_data_id = $entity_data_id;
              $found = TRUE;

              if ($depth < count($selectors) - 1) {
                // Set breadcrumb.
                $entity = array_shift($entity_types_info[$layer->entity_type]['#entity_get']->call($index, $entity_types_info[$layer->entity_type]['#primary_key'], array($root_entity_data_id), 1));
                $breadcrumbs[] = l($entity->title(), $index->contexts['page']->path . '/' . implode('/', array_slice($selectors, 0, $depth + 1)));

                // We recursively loop through the layers, so go down one level.
                $layers = $layer->layers;
                break;
              }
            }
          }
          if (!$found) {
            return MENU_NOT_FOUND;
          }
        }
        else {
          return MENU_NOT_FOUND;
        }
      }
      if ($root_layer) {
        $index->contexts['page']->browsable_path = $selectors_path;

        $root_entity = array_shift($entity_types_info[$root_layer->entity_type]['#entity_get']->call($index, $entity_types_info[$root_layer->entity_type]['#primary_key'], array($root_entity_data_id), 1));
        // Throw a 404 if no root entity has been found.
        if (!$root_entity) {
          drupal_not_found();
          exit;
        }

        // Set the page title, description and breadcrumb.
        drupal_set_title(check_plain($root_entity->title()));
        $index->description = $root_entity->description();
        drupal_set_breadcrumb($breadcrumbs);

        return array($root_layer, $root_entity);
      }
    }
  }
  return FALSE;
}

/**
 * Configure an entity's URL path.
 */
function index_display_browsable_entity_preprocess(indexEntity $entity) {
  if ($entity->child_count) {
    $entity->path = $entity->index->contexts['page']->path;
    if (isset($entity->index->contexts['page']->browsable_path)) {
      $entity->path .= '/' . $entity->index->contexts['page']->browsable_path;
    }
    $entity->path .= '/' . $entity->eid;
  }
}