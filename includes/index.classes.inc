<?php

/**
 * @file
 *   Define all Index' classes.
 */

/**
 * The base class for an index.
 *
 * @see index_load()
 *
 * @param $properties array
 *   An associative array to fill the index' properties.
 */
class index {
  public $iid = 0;
  public $title = '';
  public $description = '';
  public $cache_lifetime = 0;
  // Developers are encouraged to use $index->language() to get the language to
  // use for an index.
  public $language = 'zxx';
  public $layers = array();
  public $contexts = array();

  /**
   * Construct the object with basic properties or properties that have been
   * passed on.
   *
   * @param $properties
   *   An array with properties and their values.
   */
  function __construct(array $properties = array()) {
    $contexts_info = index_contexts_info();
    foreach ($contexts_info as $context_name => $context_info) {
      if ($context_info['#class'] != 'indexContext') {
        $this->contexts[$context_name] = new $context_info['#class']($this);
      }
      else {
        $this->contexts[$context_name] = new indexContext($context_name, $this);
      }
    }
    // Fill all properties.
    foreach ($properties as $property => $value) {
      $this->$property = $value;
    }
  }

  /**
   * Return the language to use for this index.
   *
   * @return mixed
   *    Return a two-letter language code or FALSE if no language has been
   *   defined.
   */
  function language() {
    global $language;

    if ($this->language == INDEX_LANGUAGE_ACTIVE) {
      return $language->language;
    }
    return $this->language;
  }

  /**
   * Set layers for this index.
   *
   * @param $layers array
   *   An array with indexLayer objects.
   */
  function setLayers(array $layers) {
    $this->layers = array();
    foreach ($layers as $layer) {
      $layer->index = $this;
      $parent_lineage = substr($layer->lineage, 0, strrpos($layer->lineage, '_'));
      if (strlen($parent_lineage) == 0) {
        $this->layers[] = $layer;
      }
      else {
        $layers[$parent_lineage]->layers[] = $layer;
        $layer->parent = $layers[$parent_lineage];
      }
    }
  }

  /**
   * Insert a new index into the database.
   */
  function insert() {
    $status = drupal_write_record('index_index', $this);
    $this->insertLayers();
    foreach ($this->contexts as $context) {
      $context->insert();
    }
    module_invoke_all('index_insert', $this);

    return $status;
  }

  /**
   * Update an existing index.
   */
  function update() {
    $index_old = index_load($this->iid);
    $this->deleteLayers();
    $this->insertLayers();
    $status = drupal_write_record('index_index', $this, 'iid');
    foreach ($this->contexts as $context) {
      $context->update($index_old);
    }
    module_invoke_all('index_update', $index_old, $this);
    cache_clear_all('index_entities-' . $this->iid, 'cache', TRUE);

    return $status;
  }

  /**
   * Delete the index.
   */
  function delete() {
    module_invoke_all('index_delete', $this);
    foreach ($this->contexts as $context) {
      $context->delete();
    }
    db_query("DELETE FROM {index_index} WHERE iid = %d", $this->iid);
    unset($this->iid);
  }

  /**
   * Delete this index' layers.
   */
  function deleteLayers() {
    db_query("DELETE FROM {index_layer} WHERE iid = %d", $this->iid);
  }

  /**
   * Insert this index' layers.
   */
  function insertLayers($layers = NULL) {
    $layers = !is_null($layers) ? $layers : $this->layers;
    foreach ($layers as $layer) {
     db_query("INSERT INTO {index_layer} (iid, lineage, entity_type, entity_count_only) VALUES (%d, '%s', '%s', %d)", $this->iid, $layer->lineage, $layer->entity_type, $layer->entity_count_only);
      $this->insertLayers($layer->layers);
    }
  }
}

class indexLayer {
  public $index = NULL;
  public $lineage = NULL;
  public $parent = NULL;
  public $entity_type = INDEX_NO_ENTITY_TYPE;
  public $entity_count_only = FALSE;
  public $hide_childless_entities = FALSE;
  public $layers = array();
  public $entities = array();

  /**
   * Fill all properties.
   *
   * @param $properties
   *   An array with properties and their values.
   */
  function __construct(array $properties) {
    foreach ($properties as $property => $value) {
      $this->$property = $value;
    }
  }

  /**
   * Get the entities for this layer.
   *
   * @param $context indexContext
   *   The context to get entities for.
   * @param $depth_max integer
   *   The maximum depth for which to get entities.
   * @param $depth_current integer
   *   The current layer depth.
   */
  function getEntities(indexContext $context, $depth_max, $depth_current = 1, $key = NULL, array $values = array()) {
    $this->entities[$context->name] = array();
    $entity_type_info = index_entity_type_info($this->entity_type);

    // If there are entities for a parent layer, compute the conditions for
    // which to get this layer's entities. A parent layer only has entities if
    // $key passed on to this function is NULL.
    if ($this->parent && isset($this->parent->entities[$context->name]) && count($this->parent->entities[$context->name])) {
      $key = $entity_type_info['#relations'][$this->parent->entity_type];
      $values = array();
      foreach ($this->parent->entities[$context->name] as $entity) {
        if ($entity->data->$key) {
          $values[] = $entity->data->$key;
        }
      }
    }

    // We need to get the entity count only.
    if ($key && $this->entity_count_only) {
      $counts = $entity_type_info['#entity_count']->call($this->index, $key, $values, $depth_max);
      foreach ($this->parent->entities[$context->name] as $parent_entity) {
        if (is_null($parent_entity->child_count)) {
          $parent_entity->child_count = 0;
        }
        if (isset($counts[$parent_entity->data->$key])) {
          $parent_entity->child_count += $counts[$parent_entity->data->$key];
        }
      } 
    }
    // We need to get entities.
    else {
      $this->entities[$context->name] = $entity_type_info['#entity_get']->call($this->index, $key, $values, $depth_max);
      foreach ($this->entities[$context->name] as $entity) {
        $entity->index = $this->index;
      }
      // Assign parents and children.
      if ($this->parent && isset($this->parent->entities[$context->name])) {
        foreach ($this->parent->entities[$context->name] as $parent_entity) {
          if (is_null($parent_entity->child_count)) {
            $parent_entity->child_count = 0;
          }
          foreach ($this->entities[$context->name] as $entity) {
            if (!$entity->internal_parents && $entity->data->$key == $parent_entity->data->$key) {
              $parent_entity->children[$entity->eid] = $entity;
              $parent_entity->child_count++;
              $entity->parents[$parent_entity->eid] = $parent_entity;
            }
          }
        }
      }
    }

    // Get entities for this layer's child layers.
    if (count($this->entities[$context->name]) && ($depth_max == 0 || $depth_max > $depth_current)) {
      foreach ($this->layers as $layer) {
        $layer->getEntities($context, $depth_max, $depth_current + 1);
      }
    }
    // Get entity counts for another layer below the desired depth to provide
    // child counts for the entities at the desired depth.
    else {
      foreach ($this->layers as $layer) {
        $entity_count_only = $layer->entity_count_only;
        $layer->entity_count_only = TRUE;
        $layer->getEntities($context, $depth_max, $depth_current + 1);
        $layer->entity_count_only = $entity_count_only;
      }
    }

    // Sort child entities.
    foreach ($this->entities[$context->name] as $entity) {
      uasort($entity->children, 'index_sort_entities');
    }

    // Preprocess entities.
    $display_info = index_display_info($context->display);
    if ($display_info['#entity_preprocess']) {
      foreach ($this->entities[$context->name] as $entity) {
        $display_info['#entity_preprocess']->call($entity);
      }
    }

    // Remove childless entities from their parents.
    if ($this->hide_childless_entities) {
      foreach ($this->entities[$context->name] as $entity) {
        if ($entity->child_count == 0) {
          foreach ($entity->parents as $parent_entity) {
            unset($parent_entity->children[$entity->eid]);
          }
        }
      }
    }
  }
}

abstract class indexEntity {
  public $eid = NULL;
  public $entity_type = NULL;
  public $index = NULL;
  // TRUE if this entity its parent entities in the same layer, e.g. of the
  // same entity type. This is the case with Taxonomy terms, for instance.
  public $internal_parents = FALSE;
  // The Drupal entity data, usually a stdClass instance.
  public $data = NULL;
  public $parents = array();
  public $children = array();
  // The amount of child entities. This is always precalculated, because
  // although those child entities may be there, they may not have been loaded.
  // NULL indicates there are no child layers or possible children.
  public $child_count = NULL;
  public $path = NULL;

  /**
   * Return the entity's human readable title or name.
   */
  abstract function title();

  /**
   * Return the entity's description.
   */
  abstract function description();

  function __construct($data) {
    $this->data = $data;
    $entity_type_info = index_entity_type_info($this->entity_type);
    $this->eid = $this->entity_type . '-' . $data->{$entity_type_info['#primary_key']};
  }

  /**
   * Theme this entity.
   */
  function theme() {
    $entity_type_info = index_entity_type_info($this->entity_type);
    $theme = $entity_type_info['#theme'];

    return $theme->call($this);
  }
}

/**
 * User entities.
 */
class indexEntityUser extends indexEntity {
  public $entity_type = 'user';

  function __construct($data) {
    parent::__construct($data);

    if (user_access('access user profiles')) {
      $this->path = 'user/' . $this->data->uid;
    }
  }

  function title() {
    return $this->data->name;
  }

  function description() {
    return NULL;
  }
}

/**
 * User role entities.
 */
class indexEntityUserRole extends indexEntity {
  public $entity_type = 'user_role';

  function title() {
    return $this->data->name;
  }

  function description() {
    return NULL;
  }
}

/**
 * User role entities.
 */
class indexEntityLanguage extends indexEntity {
  public $entity_type = 'language';

  function title() {
    global $language;

    if ($this->index->language() != INDEX_LANGUAGE_NEUTRAL && $this->data->language == $language->language) {
      return $this->data->native;
    }
    return $this->data->name;
  }

  function description() {
    return NULL;
  }
}

/**
 * User role entities.
 */
class indexEntityFile extends indexEntity {
  public $entity_type = 'file';

  function __construct($data) {
    parent::__construct($data);

    if (variable_get('file_downloads', FILE_DOWNLOADS_PUBLIC) == FILE_DOWNLOADS_PRIVATE) {
      $headers = module_invoke_all('file_download', $this->data->filepath);
      if (!in_array(-1, $headers)) {
        $this->path = $this->data->filepath;
      }
    }
  }

  function title() {
    return $this->data->filename;
  }

  function description() {
    return NULL;
  }
}

/**
 * Node (content) entities.
 */
class indexEntityNode extends indexEntity {
  public $entity_type = 'node';

  function __construct($data) {
    parent::__construct($data);

    if (user_access('access content')) {
      $this->path = 'node/' . $this->data->nid;
    }
  }

  function title() {
    return $this->data->title;
  }

  function description() {
    return node_view($this->data, TRUE, TRUE);
  }
}

/**
 * Node type (content type) entities.
 */
class indexEntityNodeType extends indexEntity {
  public $entity_type = 'node_type';

  function __construct($data) {
    parent::__construct($data);

    if (user_access('create ' . $this->data->type . ' content')) {
      $this->path = 'node/add/' . $this->data->type;
    }
  }

  function title() {
    return $this->data->name;
  }

  function description() {
    return $this->data->description;
  }
}

/**
 * Comment entities.
 */
class indexEntityComment extends indexEntity {
  public $entity_type = 'comment';

  function title() {
    return $this->data->subject;
  }

  function description() {
    return $this->data->comment;
  }
}

/**
 * Taxonomy term entities.
 */
class indexEntityTaxonomyTerm extends indexEntity {
  public $entity_type = 'taxonomy_term';

  function __construct($data) {
    parent::__construct($data);

    $this->path = taxonomy_term_path($this->data);
  }

  function title() {
    return $this->data->name;
  }

  function description() {
    return $this->data->description;
  }
}

/**
 * Taxonomy vocabulary entities.
 */
class indexEntityTaxonomyVocabulary extends indexEntity {
  public $entity_type = 'taxonomy_vocabulary';

  function title() {
    return $this->data->name;
  }

  function description() {
    return $this->data->description;
  }
}

/**
 * A callback that can be called from anywhere in the code.
 *
 * @param $function string
 *   The name of the function to call.
 * @param $function $file
 *   The file the function is located in within a module's directory.
 * @param $module string
 *   The name of the module the function belongs to. Only necessary if $file
 *   has been set.
 */
class indexCallback {
  public $function = NULL;
  public $file = NULL;
  public $module = NULL;

  function __construct($function, $file = NULL, $module = NULL) {
    $this->function = $function;
    $this->file = $file;
    $this->module = $module;
  }

  /**
   * Include the file the function is located in.
   */
  protected function includeFile() {
    if ($this->file) {
      require_once('./'. drupal_get_path('module', $this->module) . '/' . $this->file);
    }
  }

  /**
   * Call this callback.
   *
   * @param ...
   *   Arguments to pass on to the callback.
   */
  function call() {
    $this->includeFile();
    $args = func_get_args();

    return call_user_func_array($this->function, $args);
  }
}

/**
 * A theme callback that can be called from anywhere in the code.
 *
 * @param $theme string
 *   The theme's name.
 * @param $function $file
 *   The file the function is located in within a module's directory if this is
 *   a theme callback and no template file.
 * @param $module string
 *   The name of the module the theme belongs to. Only necessary if $file has
 *   been set.
 */
class indexThemeCallback extends indexCallback {
  function call() {
    $this->includeFile();
    $args = func_get_args();
    $args = array_merge(array($this->function), $args);

    return call_user_func_array('theme', $args);
  }
}

/**
 * A context in which an index can appear, e.g. a page or a block.
 */
class indexContext {
  public $name = NULL;
  public $index = NULL;
  public $enabled = FALSE;
  public $display = NULL;
  public $entities = array();
  public $path = NULL;

  /**
   * Define this context and fill additional properties.
   *
   * @param $name string
   *   The machine name of this context, e.g. 'page' or 'block'.
   * @param $index index
   *   The index this context belongs to.
   * @param $properties
   *   An array with properties and their values.
   */
  function __construct($name, $index, array $properties = array()) {
    $this->name = $name;
    $this->index = $index;
    foreach ($properties as $property => $value) {
      $this->$property = $value;
    }
  }

  /**
   * Create a CID for caching this context.
   */
  function cid() {
    return 'index_entities-' . $this->index->iid . '-' . $this->name . '-';
  }

  /**
   * Get cached entities.
   *
   * @return Either an array of entities or FALSE if caching is disabled or if
   *   no cached data was found.
   */
  function cacheGetEntities() {
    if ($this->index->cache_lifetime > 0 && $result = cache_get($this->cid())) {
      return $result->data;
    }
    return FALSE;
  }

  /**
   * Cache the entities if necessary.
   */
  function cacheSetEntities() {
    if ($this->index->cache_lifetime > 0) {
      cache_set($this->cid(), $this->entities, 'cache', time() + $this->index->cache_lifetime);
    }
  }

  /**
   * Insert this context into the database.
   */
  function insert() {
    db_query("INSERT INTO {index_context} (iid, name, enabled, display) VALUES (%d, '%s', %d, '%s')", $this->index->iid, $this->name, $this->enabled, $this->display);
  }

  /**
   * Update this context's database entry.
   */
  function update($index_old) {
    $this->delete();
    $this->insert();
  }

  /**
   * Delete this context from the database.
   */
  function delete() {
    db_query("DELETE FROM {index_context} WHERE iid = %d AND name = '%s'", $this->index->iid, $this->name);
  }

  /**
   * View the index for this context.
   *
   * @param $reset boolean
   *   TRUE to reset all cached data and view the context from scratch.
   *
   * @return
   *   The rendered index.
   */
  function view($reset = FALSE) {
    $this->getEntities($reset);
    index_load_include('view');
    $display_info = index_display_info($this->display);
    $content = $display_info['#theme']->call($this->index, $this);

    return theme(array('index__' . $this->index->iid, 'index'), $this->index, $this, $content);
  }

  /**
   * Get the entities for all layers of this index.
   */
  function getEntities($reset = FALSE) {
    $entity_types_info = index_entity_types_info();

    $this->entities = array();
    if (!$reset && $cached_entities = $this->cacheGetEntities()) {
      $this->entities = $cached_entities;
    }
    else {
      $display_info = index_display_info($this->display);

      // Preparatory work in case we are displaying non-root layers.
      if ($display_info['#root_entity'] && $root_info = $display_info['#root_entity']->call($this->index)) {
        // Throw a 404 if no information about a potential root entity has been
        // found.
        if ($root_info == MENU_NOT_FOUND) {
          drupal_not_found();
          exit;
        }
        list($root_layer, $root_entity) = $root_info;
        $layers = $root_layer->layers;
      }
      else {
        $layers = $this->index->layers;
      }

      foreach ($layers as $layer) {
        $values = array();
        // Define hierarchy conditions.
        if (isset($root_layer)) {
          $key = $entity_types_info[$layer->entity_type]['#relations'][$layer->parent->entity_type];
          $values = array($root_entity->data->$key);
        }
        else {
          $key = NULL;
        }

        // Delegate getting entities to the layers and store them in $this.
        $layer->getEntities($this, $display_info['#depth'], 1, $key, $values);
        foreach ($layer->entities[$this->name] as $entity) {
          if (!$entity->parents) {
            $this->entities[] = $entity;
          }
        }
      }
      if (!$reset) {
        $this->cacheSetEntities();
      }
    }
  }
}

/**
 * A page context.
 */
class indexContextPage extends indexContext {
  public $roles = array();

  /**
   * Define this context and fill additional properties.
   *
   * @param $index index
   *   The index this context belongs to.
   * @param $properties
   *   An array with properties and their values.
   */
  function __construct($index, array $properties = array()) {
    parent::__construct('page', $index, $properties);
  }

  function cid() {
    return parent::cid() . implode('_', $this->roles);
  }

  function insert() {
    parent::insert();
    db_query("INSERT INTO {index_context_page_path} (iid, path) VALUES (%d, '%s')", $this->index->iid, $this->path);
    foreach ($this->roles as $rid) {
     db_query("INSERT INTO {index_context_page_role} (iid, rid) VALUES (%d, %d)", $this->index->iid, $rid);
    }

    if ($this->enabled) {
      menu_rebuild();
    }
  }
  
  function update($index_old) {
    global $base_root, $base_path;

    parent::update($index_old);
    $this->delete();
    $this->insert();

    // Clear any pages that may have been cached for this index.
    cache_clear_all($base_root . $base_path . $this->path, 'cache_page', TRUE);
    if ($this->enabled != $index_old->contexts['page']->enabled || $this->path != $index_old->contexts['page']->path) {
      menu_rebuild();
    }
  }
  
  function delete() {
    parent::delete();
    db_query("DELETE FROM {index_context_page_path} WHERE iid = %d", $this->index->iid);
    db_query("DELETE FROM {index_context_page_role} WHERE iid = %d", $this->index->iid);

    if ($this->enabled) {
      menu_rebuild();
    }
  }
}