<?php

/**
 * @file
 *   Get and count entities that are defined in index_index_entity_info().
 */

/**
 * Construct a single SQL WHERE clause condition.
 *
 * @param $column string
 *   An SQL table column.
 * @param $values array
 *   An array containing values the column should match. Only integers,
 *   booleans and strings are allowed.
 *
 * @return string
 *   An SQL WHERE clause condition that can be directly injected into a query.
 */
function index_entity_query_condition($column, array $values, $table = NULL) {
  static $types = array(
    'integer' => 'int',
    'boolean' => 'int',
    'string' => 'text',
  );


  if ($values) {
    $column = $table ? "$table.$column" : $column;
    $condition = "$column IN (" . db_placeholders($values, $types[gettype($values[0])]) . ')';
    // Substitute placeholders here, so the calling function doesn't have to pass
    // on condition values to the query.
    _db_query_callback($values, TRUE);

    return preg_replace_callback(DB_QUERY_REGEXP, '_db_query_callback', $condition);
  }
  return NULL;
}

/**
 * Construct an SQL WHERE clause based on layer conditions.
 *
 * @param $conditions array
 *   Keys are SQL table columns and values are arrays containing values the
 *   columns should match.
 * @param $table string
 *   An optional table name to prefix column names with.
 *
 * @return string
 *   An SQL WHERE clause that can be directly injected into a query.
 */
function index_entity_query_conditions(array $conditions, $table = NULL) {
  $query_parts = array();
  foreach ($conditions as $column => $values) {
    if ($query_part = index_entity_query_condition($column, $values, $table)) {
      $query_parts[] = $query_part;
    }
  }

  if ($query_parts) {
    return ' WHERE ' . implode(' AND ', $query_parts);
  }
  return NULL;
}

/**
 * Get entities.
 *
 * @return array
 */
function index_entity_get($class, $table, $key, array $values) {
  $result = db_query("SELECT * FROM {$table}" . index_entity_query_conditions(array($key => $values)));
  $entities = array();
  while ($data = db_fetch_object($result)) {
    $entities[] = new $class($data);
  }

  return $entities;
}

/**
 * Count entities.
 *
 * @return integer
 */
function index_entity_count($table, $key, array $values) {
  $where = $key ? index_entity_query_conditions(array($key => $values)) . " GROUP BY $key" : NULL;
  $result = db_query("SELECT COUNT(*) as count, $key as value FROM {$table} $where");
  $counts = array();
  while ($count_data = db_fetch_array($result)) {
    $counts[$count_data['value']] = $count_data['count'];
  }

  return $counts;
}

/**
 * Get node types.
 *
 * @return array
 *   Node types from {node_type}.
 */
function index_entity_get_node_type(index $index, $key, array $values, $depth_max) {
  return index_entity_get('indexEntityNodeType', 'node_type', $key, $values);
}

/**
 * Count node types.
 *
 * @return integer
 */
function index_entity_count_node_type(index $index, $key, array $values) {
  return index_entity_count('node_type', $key, $values);
}

/**
 * Get nodes.
 *
 * @return array
 *   Nodes from {node} and {node_revision}.
 */
function index_entity_get_node(index $index, $key, array $values, $depth_max) {
  $entities = array();

  $query = 'SELECT u.*, n.* FROM {node} n INNER JOIN {users} u ON u.uid = n.uid INNER JOIN {node_revisions} r ON r.vid = n.vid';
  $table = 'n';

  // Get nodes by TID.
  if ($key == 'tid') {
    $query .= ' LEFT JOIN {term_node} tn ON tn.vid = n.vid';
    $table = 'tn';
  }

  $conditions = array($key => $values);
  if ($index->language() != INDEX_LANGUAGE_NEUTRAL) {
    $conditions['language'] = array($index->language());
  }
  $result = db_query($query . index_entity_query_conditions($conditions, $table));
  while ($data = db_fetch_object($result)) {
    $entities[] = new indexEntityNode($data);
  }

  return $entities;
}

/**
 * Count nodes.
 *
 * @return integer
 */
function index_entity_count_node(index $index, $key, array $values) {
  $query = "SELECT COUNT(*) as count, n.$key as value FROM {node} n";
  $table = 'n';
  $counts = array();

  // Get nodes by TID.
  if ($key == 'tid') {
    $query .= ' LEFT JOIN {term_node} tn ON tn.vid = n.vid';
    $table = 'tn';
  }

  $conditions = array($key => $values);
  if ($index->language() != INDEX_LANGUAGE_NEUTRAL) {
    $conditions['language'] = array($index->language());
  }
  $result = db_query($query . index_entity_query_conditions($conditions, $table) . " GROUP BY n.$key");
  while ($count_data = db_fetch_array($result)) {
    $counts[$count_data['value']] = $count_data['count'];
  }

  return $counts;
}

/**
 * Get comments.
 *
 * @return array
 *   Comments from {comments}.
 */
function index_entity_get_comment(index $index, $key, array $values, $depth_max) {
  // TODO: Comments are hierarchical as well.
  return index_entity_get('indexEntityComment', 'comments', $key, $values);
}

/**
 * Count comments.
 *
 * @return integer
 */
function index_entity_count_comment(index $index, $key, array $values) {
  return index_entity_count('comments', $key, $values);
}

/**
 * Get users.
 *
 * @return array
 *   Users from {users}.
 */
function index_entity_get_user(index $index, $key, array $values, $depth_max) {
  $where = ($clause = index_entity_query_conditions(array($key => $values))) ? "$clause AND uid <> 0" : ' WHERE uid <> 0';
  $result = db_query("SELECT * FROM {users}" . $where);
  $entities = array();
  while ($data = db_fetch_object($result)) {
    $entities[] = new indexEntityUser($data);
  }

  return $entities;
}

/**
 * Count users.
 *
 * @return integer
 */
function index_entity_count_user(index $index, $key, array $values) {
  return index_entity_count('users', $key, $values);
}

/**
 * Get files.
 *
 * @return array
 *   Files from {files}.
 */
function index_entity_get_file(index $index, $key, array $values, $depth_max) {
  $entities = array();

  $query = 'SELECT * FROM {files} f';
  $table = 'f';

  // Get files by VID.
  if ($key == 'vid') {
    $query .= ' LEFT JOIN {upload} u ON f.fid = u.fid';
    $table = 'u';
  }

  $result = db_query($query . index_entity_query_conditions(array($key => $values), $table));
  while ($data = db_fetch_object($result)) {
    $entities[] = new indexEntityFile($data);
  }

  return $entities;
}

/**
 * Count files.
 *
 * @return integer
 */
function index_entity_count_file(index $index, $key, array $values) {
  $query = "SELECT COUNT(*) as count, f.$key as value FROM {files} f";
  $counts = array();

  // Get files by NID.
  if ($key == 'nid') {
    $query .= ' LEFT JOIN {upload} u ON f.fid = u.fid';
  }

  $result = db_query($query . index_entity_query_conditions(array($key => $values), 'f') . " GROUP BY f.$key");
  while ($count_data = db_fetch_array($result)) {
    $counts[$count_data['value']] = $count_data['count'];
  }

  return $counts;
}

/**
 * Get user roles.
 *
 * @return array
 *   User roles from {users_roles}. 
 */
function index_entity_get_user_role(index $index, $key, array $values, $depth_max) {
  $result = db_query("SELECT * FROM {role} r LEFT JOIN {users_roles} ur ON ur.rid = r.rid" . index_entity_query_conditions(array($key => $values), 'r'));
  $entities = array();
  while ($user_role = db_fetch_object($result)) {
    $entities[] = new indexEntityUserRole($user_role);
  }

  return $entities;
}

/**
 * Count user roles.
 *
 * @return integer
 */
function index_entity_count_user_role(index $index, $key, array $values) {
  $result = db_query("SELECT COUNT(*) FROM {role} r LEFT JOIN {users_roles} ur ON ur.rid = r.rid" . index_entity_query_conditions(array($key => $values)));

  return db_result($result);
}

/**
 * Get Taxonomy vocabularies.
 *
 * @return array
 *   Vocabularies from {vocabulary}.
 */
function index_entity_get_taxonomy_vocabulary(index $index, $key, array $values, $depth_max) {
  return index_entity_get('indexEntityTaxonomyVocabulary', 'vocabulary', $key, $values);
}

/**
 * Count Taxonomy vocabularies.
 *
 * @return integer
 */
function index_entity_count_taxonomy_vocabulary(index $index, $key, array $values, $depth_max) {
  return index_entity_count('vocabulary', $key, $values);
}

/**
 * Get Taxonomy terms.
 *
 * @return array
 *   Terms from {term_data}.
 */
function index_entity_get_taxonomy_term(index $index, $key, array $values, $depth_max) {
  $entities = array();
  $where = index_entity_query_conditions(array($key => $values));

  // Get terms by VID.
  if ($key == 'vid') {
    foreach ($values as $vid) {
      $tree = taxonomy_get_tree($vid, 0, -1, $depth_max ? $depth_max : NULL);
      $entities += index_entity_get_taxonomy_term_build($tree);
    }
  }

  // Get terms by NID.
  elseif ($key == 'nid') {
    // Get terms using Taxonomy's API, because we need multiple levels.
    if ($depth_max) {
      $result = db_query("SELECT td.tid, td.vid, tn.nid FROM {term_data} td LEFT JOIN {term_node} tn ON td.tid = tn.tid $where");
      while ($term_data = db_fetch_array($result)) {
        $tree = taxonomy_get_tree($term_data['vid'], $term_data['tid'], -1, $depth_max ? $depth_max : NULL);
        $entities += index_entity_get_taxonomy_term_build($tree);
      }
    }
    // If we only need the first level, use a custom query.
    else {
      $result = db_query("SELECT td.*, tn.nid FROM {term_data} td LEFT JOIN {term_node} tn ON td.tid = tn.tid $where");
      while ($term = db_fetch_object($result)) {
        $entities[] = new indexEntityTaxonomyTerm($term);
        unset($term->nid);
      }
    }
  }

  // Get terms by TID. This is the case if we want a term's children.
  elseif ($key == 'tid') {
    $result = db_query("SELECT tid, vid FROM {term_data} $where");
    while ($term_data = db_fetch_array($result)) {
      $tree = taxonomy_get_tree($term_data['vid'], $term_data['tid'], -1, $depth_max ? $depth_max : NULL);
      $entities += index_entity_get_taxonomy_term_build($tree);
    }
  }

  // Load all terms if there are no conditions.
  else {
    foreach (array_keys(taxonomy_get_vocabularies()) as $vid) {
      $tree = taxonomy_get_tree($vid, 0, -1, $depth_max ? $depth_max : NULL);
      $entities += index_entity_get_taxonomy_term_build($tree);
    }
  }

  return $entities;
}

/**
 * Build an entity hierarchy from a taxonomy_get_tree() result.
 *
 * @param $tree array
 *   A taxonomy_get_tree() result.
 */
function index_entity_get_taxonomy_term_build(array $tree) {
  $entities = array();
  // Use TIDs as keys for easy access.
  foreach ($tree as $term) {
    $entities[$term->tid] = new indexEntityTaxonomyTerm($term);
  }
  // Assign parents and children.
  foreach ($entities as $entity) {
    $entity->data->parents = array_filter($entity->data->parents);
    if ($entity->data->parents) {
      $entity->internal_parents = TRUE;
      foreach ($entity->data->parents as $tid) {
        // TODO: This probably won't work when selecting only part of the tree.
        $entity->parents[] = $entities[$tid];
        $entities[$tid]->children[] = $entity;
        $entities[$tid]->child_count++;
      }
    }
  }

  return array_values($entities);
}

/**
 * Count Taxonomy terms.
 *
 * @return integer
 */
function index_entity_count_taxonomy_term(index $index, $key, array $values, $depth_max) {
  return index_entity_count('term_data', $key, $values);
}