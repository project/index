<?php

/**
 * Indicate Index is being installed during a page load. Required because
 * drupal_write_record() in $index->insert() requires index_schema(), which is
 * not being called during hook_install(). Using this global $index->insert()
 * can be called in index_enable().
 *
 * @see index_install()
 * @see index_enable()
 */
global $index_install;

/**
 * Declare display modes.
 *
 * @see theme_index_display_browsable()
 * @see index_display_browsable_root_entity()
 * @see index_display_browsable_entity_preprocess()
 *
 * @return array
 *   An array where the keys are the display modes' machine names and the
 *   values are arrays with the following keys:
 *   - #title (required): the human-readable name of the display mode.
 *   - #contexts (required): the machine names of the contexts ('page',
 *     'block', etc.) this display mode can work with.
 *   - #page (optional): TRUE if this display mode can be used for index pages.
 *     Defaults to FALSE.
 *   - #block (optional): TRUE if this display mode can be used for index
 *     blocks. Defaults to FALSE.
 *   - #depth (optional): how many layers to get entities for when displaying
 *     them using this display mode. 0 means all available layers. Defaults to
 *     0.
 *   - #theme (required): An indexThemeCallback instance to theme an index
 *     using this display.
 *   - #root_entity (optional): An indexCallback instance to get the root
 *     entity under which to render the index. It should return an array where
 *     the first item is a reference to a layer and the second is the ID of an
 *     entity in that layer.
 *   - #entity_preprocess (optional): An indexCallback instance to manipulate
 *     entities before they are passed on to theme functions.
 */
function hook_index_display_info() {
  $modes['browsable'] = array(
    '#title' => t('Browsable'),
    '#contexts' => array('page'),
    '#depth' => 1,
    '#theme' => new indexThemeCallback('index_display_browsable', 'includes/index.view.inc'),
    '#root_entity' => new indexCallback('index_display_browsable_root_entity', 'includes/index.view.inc'),
    '#entity_preprocess' => new indexCallback('index_display_browsable_entity_preprocess', 'includes/index.view.inc'),
  );
}

/**
 * Alter declared display modes.
 */
function hook_index_display_info_alter(array &$modes) {
  $modes['tree']['#block'] = FALSE;
}

/**
 * Declare entity types.
 *
 * @return array
 *   An array where the keys are the entity types' machine names and the values
 *   are arrays with the following keys:
 *   - #title (required): the human-readable name of the entity type.
 *   - #relations (required): an array with definitions of relations with other
 *     entity types. The keys are the machine names of other entity types and
 *     the keys are the common fields/properties with those types.
 *   - #entity_getcallback (required): An indexCallback instance that returns
 *     an array with instances of a class that extends indexEntity.
 *   - #entity_count (required): An indexCallback instance.
 *   - #theme (required): An indexThemeCallback instance.
 */
function hook_index_entity_type_info() {
  $entities['node'] = array(
    '#title' => t('Content'),
    '#relations' => array(
      'user' => 'uid',
      'comment' => 'nid',
      'taxonomy_term' => 'tid',
      'node_type' => 'type',
    ),
    '#entity_get' => new indexCallback('index_entity_get_node', 'includes/index.entity.inc'),
    '#entity_count' => new indexCallback('index_entity_count_node', 'includes/index.entity.inc'),
    '#theme' => new indexThemeCallback('index_entity_node', 'includes/index.view.inc'),
  );

  return $entities;
}

/**
 * Alter declared entity types.
 */
function hook_index_entity_type_info_alter(array &$entities) {
  $entities['node']['#relations']['foo'] = 'bar';
}

/**
 * Perform tasks when loading an index.
 *
 * Implementations of this hook should not return anything. If desired, they
 * can change $index directly, because it is passed on by reference.
 *
 * @param $index index
 *   The index that is being loaded.
 */
function hook_index_load(index $index) {
  $index->foo = 'bar';
}

/**
 * Perform tasks right before an index is being deleted.
 *
 * @param $index index
 *   The index that is being deleted.
 */
function hook_index_delete(index $index) {
  db_query("DELETE FROM {foo_index} WHERE iid = %d", $index->iid);
}

/**
 * Perform tasks when saving a new index.
 *
 * Implementations of this hook should not return anything. If desired, they
 * can change $index directly, because it is passed on by reference.
 *
 * @param $index index
 *   The index that is being saved.
 */
function hook_index_insert(index $index) {
  $index->foo = 'bar';
}

/**
 * Perform tasks when saving an existing index.
 *
 * Implementations of this hook should not return anything. If desired, they
 * can change $index directly, because it is passed on by reference.
 *
 * @param $index_old index
 *   The index before is was changed.
 * @param $index_new index
 *   The index that is being saved.
 */
function hook_index_update(index $index_old, index $index_new) {
  if ($index_new->page_enabled != $index_old->page_enabled || $index_new->page_path != $index_old->page_path) {
    menu_rebuild();
  }
}

/**
 * Perform tasks to create a blueprint of an index.
 *
 * When exporting or cloning indexes, certain site-specific data should be
 * removed from the index to prevent problems. Indexes should not be enabled
 * directly after importing them and page paths may conflict with paths on
 * another website, for instance.
 *
 * @param $blueprint index
 *   A clone of the original index that should be modified.
 * @param $index index
 *   The original index, which can be used for reference. DO NOT ALTER.
 */
function hook_index_blueprint(index $blueprint, index $index) {
  foreach ($blueprint->contexts as &$context) {
    $context->enabled = FALSE;
  }
  $blueprint->contexts['page']->path = NULL;
}