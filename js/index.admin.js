/**
 * Context tabs.
 */
Drupal.behaviors.indexAdminContextTabs = function (context) {
  var list = $('<ul class="tabs"></ul>');
  // Transform each context fieldset into a tab.
  $.each(Drupal.settings.indexContexts, function(contextName, contextTitle) {
    list.append($('<li></li>').text(contextTitle).click(function() {
      $('#index-contexts .tabs li').removeClass('active');
      $(this).addClass('active');
      $('#index-contexts fieldset').hide();
      $('#index-context-' + contextName).show();
    }));
  });

  $('#index-contexts>legend').after(list);
  // Wrap all fieldsets and hide all but the first one.
  $('#index-contexts').addClass('index-tabs').find('fieldset').hide().wrapAll('<div class="panes"></div>').filter(':first').show();
  $('#index-contexts .tabs li:first').addClass('active first');
};

/**
 * Hide and show behavior for context settings.
 */
Drupal.behaviors.indexAdminContextSettings = function (context) {
  $('.index-context-enabled:not(:checked)').each(function () {
    $(this.parentNode.parentNode.parentNode).children('.index-context-settings').hide();
  });
  $('.index-context-enabled').change(function () {
    $(this.parentNode.parentNode.parentNode).children('.index-context-settings').slideToggle();
  });
};

/**
 * Prepare the form for AJAX updates.
 */
Drupal.behaviors.indexAdminFormUpdatePrepare = function (context) {
  var throbber = '<span class="index-throbber"></span>';
  $.each(Drupal.settings.indexContexts, function(contextName, contextTitle) {
    $('#index-context-preview-' + contextName + ' > h2').html(throbber + Drupal.t('Live preview'));
  });
  $('#edit-update-preview').hide();
}

/**
 * Attach the eventhandlers for the AJAX form update.
 */
Drupal.behaviors.indexAdminFormUpdate = function (context) {
  $('.index-form-add-layer-child-add, .index-form-add-layer-remove').click(Drupal.indexAdminFormUpdate);
  $('#index-form-add input, #index-form-add textarea, #index-form-add select').change(Drupal.indexAdminFormUpdate);
	// Disable hyperlinks so form input won't be lost and users will not end up
	// at non-existent pages.
	$('.index-context-preview a').click(function() {
		return false;
	});
};

/**
 * AJAX layer update.
 */
Drupal.indexAdminFormUpdate = function() {
  var values = $('#index-form-add').serialize() + '&';
  if (this.type == 'submit') {
    values += this.name + '=' + this.value;
  }
  // If no button's value is present in $_POST FAPI will assume that the first
  // defined button has been clicked, which means a new layer gets added. To
  // prevent this we tell FAPI the preview button was clicked.
  else {
    values += 'op=' + Drupal.t('Update preview');
  }
  $.post(Drupal.settings.basePath + 'admin/build/index/form_add_update_ajax',
    values,
    function (data, textStatus) {
      if (data.layers) {
        $('#index-form-add-layers').replaceWith(data.layers);
      }
      $.each(Drupal.settings.indexContexts, function(contextName, contextTitle) {
        $('#index-context-preview-' + contextName + ' .content').html(data.previews[contextName]);
      });
      $('.index-throbber').removeClass('throbbing');
      Drupal.behaviors.indexAdminFormUpdate();
    },
    'json'
  );
  $('.index-throbber').addClass('throbbing');
  if (this.type == 'submit') {
    $('#index-form-add-layers input, #index-form-add-layers select').attr('disabled', 'disabled').addClass('disabled');
  }

  return false;
}