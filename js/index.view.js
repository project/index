/**
 * Hide and show behavior for page- and block-specific settings.
 */
Drupal.behaviors.indexDisplayTree = function (context) {
  // Expandable list items.
  $('.index-display-tree_expandable .parent').addClass('collapsed');
  $('.index-display-tree_expandable .parent>.index-list-style').click(function() {
    $(this.parentNode).toggleClass('collapsed');
  });
  // Expand items linking to the current page and their parent items.
  $('.index-display-tree_expandable a').each(function() {
    if (this.href == window.location) {
      $(this).parents('.parent').filter('.index-display-tree_expandable li').removeClass('collapsed');
    }
  });
};